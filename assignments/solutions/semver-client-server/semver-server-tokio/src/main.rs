use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::{convert::TryInto, convert::TryFrom, io};
use std::{env, io::prelude::*};

use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::{TcpListener, TcpStream};

use semver::EnumRepository;
use semver_api::{ApiError, Command};

#[tokio::main]
async fn main() -> io::Result<()> {
    let port = env::var("PORT").unwrap_or("7878".to_string());
    let addr = format!("127.0.0.1:{}", port);
    println!("serving at {}", addr);
    let listener = TcpListener::bind(addr).await.unwrap();

    let repository = Arc::new(Mutex::new(EnumRepository::new()));

    loop { 
        let mut stream = match listener.accept().await {
            Ok((stream, _)) => stream,
            Err(_) => {
                // unreachable as per documentation
                continue;
            }
        };

        let repository = repository.clone();
        tokio::spawn(async {
            let result = handle(&mut stream, &*repository).await;
            if result.is_err() {
                eprintln!("Error occurred: {:?}", result);
            }

            let response: Result<String, _> = semver_api::ApiResult(result).try_into();
            match response {
                Ok(response) => {
                    let _ = stream.write_all(format!("{}", response).as_bytes()).await;
                }
                Err(e) => {
                    eprintln!("Error occurred: {:?}", e);
                }
            };
        });
    }

    //Ok(())
}

async fn handle(
    stream: &mut TcpStream,
    repository: &Mutex<EnumRepository>,
) -> Result<Option<String>, ApiError> {
    let command = read_command(stream).await?;

    let mut repository = repository.lock().unwrap();

    let response = match command {
        Command::Get(crate_name) => {
            let crt = repository
                .get(&crate_name)
                .map_err(|e| ApiError::ParseError(format!("{:?}", e)))?;
            let s: Result<String, _> = crt
                .try_into().map_err(|_| ApiError::Internal);
            s.map(|data| Some(data))
        }
        Command::Put(crt) => {
            repository.insert(crt);
            Ok(None)
        }
        Command::Update(update) => {
            repository.add_release(update.crate_name, update.version)?;
            Ok(None)
        }
    };
    response
}

async fn read_command(stream: &mut TcpStream) -> Result<Command, ApiError> {
    let mut read_buffer = String::new();
    let mut buffered_stream = BufReader::new(stream);
    buffered_stream
        .read_line(&mut read_buffer)
        .await
        .map_err(|_| ApiError::InvalidCommand)?;
    //read_buffer.as_str().try_into()
    Command::from_str(read_buffer.as_str())
}
